# js2go

We can call JavaScript functions from Golang by generating all code we need. But how do we call Golang functions from JavaScript?

This library will do the job. It has JS/GO code generators based on your code, and object wrappers to do the job.

## Core

In order to make it work we need something to handle JS requests and push Golang events to JS. It would be a Go handler for http requests. Lets call it `App`.

```go
type App interface {
  Create() error
  Handler(w http.ResponseWriter, r *http.Request)
  Close() error
}
```

Then, we need let JS create Go objects, we need `Service` called by name (or services) to call to create objects. It would be some kind of global names or singelton objects. One name one instance.

```go
type Service interface {
  Create() error
  Run() error
  Close() error
}
```

We need place to store Go `Object` not by it's type name but by ID (GUID for example) to be able to create multiply objects with same type.

```go
type Object interface {
  // unique object string, which prevent confusion between similar objects.
  Id() string
  Create() error
  Run() error
  Close() error
}
```

To implement it, we need keep track these types of objects:

```go
// Name -> Reflect Type map. We put all background services here. So we can get it from JS.
var ServicesMap map[string]reflect.Type
// Name -> Reflect Type map. We put all JS accessable object here.
var ObjectsMap map[string]reflect.Type
// Name -> Reflect Type map. Core types for serialization, including AppType, ServiceType, ObjectType.
var TypesMap map[string]reflect.Type

// Name -> Object map
var Services = make(map[string]interface{})
// UUID -> Object map
var Objects = make(map[string]interface{})
```

## Listeners

To be able to send callbacks back to JS we need to create Go object wrapper which will convert call parameters between runtimes (Golang && JS) and run EventSource pipe between Go and JS. All this magic hidden in js2go library.

JavaScript Side EventSource

```js
var request = new XMLHttpRequest();
core.es = new EventSource("/js/events/" + core.es_id);
core.es.onmessage = function(e) {
  // parse_html(e.data) and call JS objects
}
```

Go Connection EventSource
```go
es := eventsource.EventSource
es.ServeHTTP(w, r)
```

Go New Event Function Call EventSource

```go
func (m *ObjectListenerPrototype) Event1(args Struct) {
  m.js.Go2Js(m.es_id, m.object_id, "Event1", args)
}
```

Example implementation, will be cleared and pushed to this repository:

  * https://github.com/axet/terminal
